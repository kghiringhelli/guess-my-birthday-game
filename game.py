from random import randint



name = input("Hi, what is your name?")

for guess in range(1, 6):

    randomYear = randint(1924,2004)
    randomMonth = randint(1,12)

    print("Guess", guess, ":","Hi", name, "were you born in", randomMonth, "/", randomYear,"?")

    ans = input("yes or no?")

    if ans == "yes":
        print("I knew it!")
        break
    elif ans == "no":
        print("Drat! Let me try again!")
    elif guess == 5:
            print("I have better things to do. Good bye")
